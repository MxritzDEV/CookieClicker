package de.moritz.clicker;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author: toLowerCase
 */
public class Main extends JavaPlugin {

    private static Main instance;
    private static ProtocolManager protocolManager;

    @Override
    public void onLoad( ) {
        Main.instance = this;
    }

    @Override
    public void onEnable( ) {
        Main.protocolManager = ProtocolLibrary.getProtocolManager();
    }

    @Override
    public void onDisable( ) {

    }
}    